export function calculPv(base, iv, ev, niv) {
  return Math.floor(
    (iv * 1 + 2 * base + Math.floor(ev / 4)) * niv / 100 + 10 + niv,
  );
}

export function calculStat(base, iv, ev, niv, nature) {
  return Math.floor(
    Math.floor((iv + 2 * base + Math.floor(ev / 4)) * niv / 100 + 5) * nature,
  );
}

export function calculDamage(
  levelAttacker,
  attackAttacker,
  defDeffender,
  attackBase,
  modificator,
) { // modificator -> calculModif
  return Math.floor(
    (((2 * levelAttacker + 10) / 205) * (attackAttacker / defDeffender) *
        attackBase + 2) * modificator,
  );
}

export function calculModif(stab, type, critique, bonus, damageRoll) { // stab -> calculStab, damageRoll -> calculDamageRoll
  return stab * type * critique * bonus * damageRoll;
}

export function calculStab(typeAttacker1, typeAttacker2, typeAttack) { // typeAttack -> Bonus d l'attaque
  return ((typeAttacker1 === typeAttack) || (typeAttacker2 === typeAttack)
    ? 1.5
    : 1);
}

export function calculDamageRoll() {
  return Math.random() * (1.0000000000000001 - 0.85) + 0.85;
}
