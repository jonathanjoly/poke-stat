import { calculPv, calculStat } from './computations';


export const STATS = ['pv', 'att', 'def', 'attS', 'defS', 'vit'];
export function emptyStats() {

	return {
		pv: 0,
		att: 0,
		def: 0,
		attS: 0,
		defS: 0,
		vit: 0
	};
}

export function emptyPokemon() {
	return {
		num: "",
		types: [],
		image: "",
		nom: "",
		stats: {
			ivs: emptyStats(),
			evs: emptyStats(),
			base: emptyStats(),
			total: emptyStats(),
			damage: 0
		},
		attacks: [],
		nature: null,
		level: 1
	}
}

export function computeStat(stat, pokemon, ivs, evs, level, nature) {
	return stat === "pv" ?
		calculPv(pokemon.pv * 1, ivs.pv * 1, evs.pv, level * 1) :
		calculStat(pokemon[stat] * 1, ivs[stat] * 1, evs[stat] * 1, level * 1, nature[stat]);
}


