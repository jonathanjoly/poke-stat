import VueRouter from 'vue-router';
//import SelectPokemon from './components/SelectPokemon.vue';

const Pokedex = () => import('./components/PokemonList.vue');
const Attacks = () => import('./components/AttackList.vue');
const Fight = () => import('./components/Fight.vue');

export const router = new VueRouter({
    routes: [
        { path: '/pokedex', name: 'pokedex', component: Pokedex },
        { path: '/fight', name: 'fight', component: Fight },
        { path: '/attacks', name: 'attacks', component: Attacks },
        { path: '/*', redirect: { name: 'fight' } }
    ]
})

