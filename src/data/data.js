import attaques from './attaques.json';
import natures from './natures.json';
import pokemons from './pokedex.json';
import types from './types.json';


export default {
    attacks: attaques,
    natures,
    pokemons,
    types
};
